let overlay = document.querySelector(".overlay");
let gaming = document.querySelector("#gaming");
let popup = document.querySelectorAll(".popup");

let overflow = false;
function showPopup(ed) {
  if (ed === "gaming") {
    gaming.classList.toggle("show-popup");
  } else {
    popup.forEach((p) => {
      p.classList.remove("show-popup");
    });
  }
  overlay.classList.toggle("active");
  overflow = !overflow;
  hideOverflowWhenOverlayIsActive();
}
overlay.addEventListener("click", () => {
  showPopup("");
});

function hideOverflowWhenOverlayIsActive() {
  if (overflow) {
    document.querySelector("body").style.overflow = "hidden";
  } else {
    document.querySelector("body").style.overflow = "visible";
  }
}
